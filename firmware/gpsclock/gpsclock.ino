// gpsclock.ino
// firmware for gps driven digital clock

#include <TinyGPS++.h> //GPS parsing

TinyGPSPlus gps;

// !!! d11-13 (atmega 17-19) are ICSP lines.  Avoid low-impedance loads on these pins.

// pins d0 and d1 used for hardware serial

// LED control pins
const int DIGIT1 = 2;    // Display pin 1
const int DIGIT2 = 10;   // Display pin 2
const int DIGIT3 = 9;    // Display pin 6
const int DIGIT4 = 6;    // Display pin 8

const int SEGA = A1;     // Display pin 14
const int SEGB = 3;      // Display pin 16
const int SEGC = 4;      // Display pin 13
const int SEGD = 5;      // Display pin 3
const int SEGE = A0;     // Display pin 5
const int SEGF = 7;      // Display pin 11
const int SEGG = 8;      // Display pin 15

const int MISCLED = A2;  // colon and am/pm positive (display pins 4 and 10)
const int COLON = A3;    // colon gnd (display pin 12)
const int AMPM = A4;     // am/pm gnd (display pin 9)

// input pins
const int HOURADJUST = 11; // button increases hour offset from UTC
const int PHOTOCELL = A5;  // senses ambient to adjust brightness

// free pins: 
// d12 (atmega 18) (ICSP) 
// d13 (atmega 19) (ICSP)


void setup() {
  Serial.begin(9600);

  // 7 seg lines
  pinMode(SEGA, OUTPUT);
  pinMode(SEGB, OUTPUT);
  pinMode(SEGC, OUTPUT);
  pinMode(SEGD, OUTPUT);
  pinMode(SEGE, OUTPUT);
  pinMode(SEGF, OUTPUT);
  pinMode(SEGG, OUTPUT);

  // dig select lines
  pinMode(DIGIT1, OUTPUT);
  pinMode(DIGIT2, OUTPUT);
  pinMode(DIGIT3, OUTPUT);
  pinMode(DIGIT4, OUTPUT);

  // non-digit lines
  pinMode(MISCLED, OUTPUT);
  pinMode(COLON, OUTPUT);  
  pinMode(AMPM, OUTPUT);

  // other lines
  pinMode(HOURADJUST, INPUT);
  pinMode(PHOTOCELL, INPUT);
}

int reading = 0;

//1000 pretty bright (5.9mA)
//500 normal (3mA)
//200 dim but readable (1.4mA)
//50 dim but readable (0.56mA)

unsigned int displayBrightness = 500;
unsigned long lastBrightnessUpdate = 0;
void refreshDisplayBrightness(void) {
  const unsigned int BRIGHTNESS_MAX = 1000;
  const unsigned int BRIGHTNESS_MIN = 1;

  reading = analogRead(PHOTOCELL);
  if (reading > 1000) {
    reading = 1000;
  } else if (reading < 0) {
    reading = 0;
  }

  displayBrightness = reading / 1000.0 * (BRIGHTNESS_MAX - BRIGHTNESS_MIN) + BRIGHTNESS_MIN;
}

unsigned long lastHourPress = 0;
unsigned char hourOffset = 0;
void hourButtonPressed(unsigned long now) {
  if (lastHourPress + 300 < now) {
    hourOffset = (hourOffset + 1) % 24;
    lastHourPress = now;
  }
}

//While we delay for a given amount of time, gather GPS data
static void smartdelay(unsigned long ms)
{
  unsigned long start = millis();

  if (lastBrightnessUpdate + 200 < start) {
    refreshDisplayBrightness();    
  }

  if (digitalRead(HOURADJUST)) {
    hourButtonPressed(start);
  }

  do 
  {
    for (int i=0; i<10; ++i) {
      if (Serial.available()) {
        gps.encode(Serial.read());
      }
    }
  } while (millis() - start < ms);
}

void loop() {
  //int time = gps.time.hour() * 100 + gps.time.minute();
  displayNumber(hourOffset*100 + hourOffset);
}

//Given a number, we display 10:22
//After running through the 4 numbers, the display is left turned off

//Display brightness
//Each digit is on for a certain amount of microseconds
//Then it is off until we have reached a total of 20ms for the function call
//Let's assume each digit is on for 1000us
//If each digit is on for 1ms, there are 4 digits, so the display is off for 16ms.
//That's a ratio of 1ms to 16ms or 6.25% on time (PWM).
//Let's define a variable called brightness that varies from:
//5000 blindingly bright (15.7mA current draw per digit)
//2000 shockingly bright (11.4mA current draw per digit)
//1000 pretty bright (5.9mA)
//500 normal (3mA)
//200 dim but readable (1.4mA)
//50 dim but readable (0.56mA)
//5 dim but readable (0.31mA)
//1 dim but readable in dark (0.28mA)

void displayNumber(int toDisplay) {
  #define DIGIT_ON  HIGH
  #define DIGIT_OFF  LOW

  long beginTime = millis();

  bool colon = ((beginTime / 1000) % 2);
  unsigned char hours = toDisplay / 100;
  unsigned char minutes = toDisplay % 100;
  bool pm = hours > 11;
  if (pm) hours -= - 12;
  if (hours == 0) hours = 12;

  // [0][1]:[2][3]
  unsigned char digits[4];
  digits[0] = hours / 10;
  digits[1] = hours % 10;
  digits[2] = minutes / 10;
  digits[3] = minutes % 10;

  for(int digit = 3 ; digit >= 0 ; digit--) {
    int currDigitPin = 0;

    switch(digit) {
    case 0:
      currDigitPin = DIGIT1;
      break;
    case 1:
      currDigitPin = DIGIT2;
      break;
    case 2:
      currDigitPin = DIGIT3;
      break;
    case 3:
      currDigitPin = DIGIT4;
      break;
    }

    unsigned char segmentsLit = lightNumber(digits[digit]);
    digitalWrite(currDigitPin, DIGIT_ON);
    
    unsigned long baseBrightness = displayBrightness / 2;
    unsigned long actualBrightness = baseBrightness + baseBrightness * segmentsLit / 7;

    if (segmentsLit) {
      delayMicroseconds(actualBrightness);
    }

    digitalWrite(currDigitPin, DIGIT_OFF);
    lightNumber(10);
  }

  unsigned char dotsLit = 0;
  if (colon) dotsLit += 2;
  if (pm) ++dotsLit;
  unsigned long baseBrightness = displayBrightness / 2;
  unsigned long actualBrightness = baseBrightness + baseBrightness * dotsLit / 7;
  digitalWrite(COLON, colon ? DIGIT_ON : DIGIT_OFF);
  digitalWrite(AMPM, pm ? DIGIT_ON : DIGIT_OFF);
  digitalWrite(MISCLED, DIGIT_ON);
  delayMicroseconds(actualBrightness);
  digitalWrite(MISCLED, DIGIT_OFF);
  
  // This is the dead time to wait between refreshes.
  smartdelay(20);
}

// Given a digit, turns on those segments
// If number is not 0 through 9, then turn off digit
// returns number of segments lit
unsigned char lightNumber(unsigned char numberToDisplay) {

#define SEGMENT_ON  LOW
#define SEGMENT_OFF HIGH

  switch (numberToDisplay) {

  case 0:
    digitalWrite(SEGA, SEGMENT_ON);
    digitalWrite(SEGB, SEGMENT_ON);
    digitalWrite(SEGC, SEGMENT_ON);
    digitalWrite(SEGD, SEGMENT_ON);
    digitalWrite(SEGE, SEGMENT_ON);
    digitalWrite(SEGF, SEGMENT_ON);
    digitalWrite(SEGG, SEGMENT_OFF);
    return 6;

  case 1:
    digitalWrite(SEGA, SEGMENT_OFF);
    digitalWrite(SEGB, SEGMENT_ON);
    digitalWrite(SEGC, SEGMENT_ON);
    digitalWrite(SEGD, SEGMENT_OFF);
    digitalWrite(SEGE, SEGMENT_OFF);
    digitalWrite(SEGF, SEGMENT_OFF);
    digitalWrite(SEGG, SEGMENT_OFF);
    return 2;

  case 2:
    digitalWrite(SEGA, SEGMENT_ON);
    digitalWrite(SEGB, SEGMENT_ON);
    digitalWrite(SEGC, SEGMENT_OFF);
    digitalWrite(SEGD, SEGMENT_ON);
    digitalWrite(SEGE, SEGMENT_ON);
    digitalWrite(SEGF, SEGMENT_OFF);
    digitalWrite(SEGG, SEGMENT_ON);
    return 5;

  case 3:
    digitalWrite(SEGA, SEGMENT_ON);
    digitalWrite(SEGB, SEGMENT_ON);
    digitalWrite(SEGC, SEGMENT_ON);
    digitalWrite(SEGD, SEGMENT_ON);
    digitalWrite(SEGE, SEGMENT_OFF);
    digitalWrite(SEGF, SEGMENT_OFF);
    digitalWrite(SEGG, SEGMENT_ON);
    return 5;

  case 4:
    digitalWrite(SEGA, SEGMENT_OFF);
    digitalWrite(SEGB, SEGMENT_ON);
    digitalWrite(SEGC, SEGMENT_ON);
    digitalWrite(SEGD, SEGMENT_OFF);
    digitalWrite(SEGE, SEGMENT_OFF);
    digitalWrite(SEGF, SEGMENT_ON);
    digitalWrite(SEGG, SEGMENT_ON);
    return 4;

  case 5:
    digitalWrite(SEGA, SEGMENT_ON);
    digitalWrite(SEGB, SEGMENT_OFF);
    digitalWrite(SEGC, SEGMENT_ON);
    digitalWrite(SEGD, SEGMENT_ON);
    digitalWrite(SEGE, SEGMENT_OFF);
    digitalWrite(SEGF, SEGMENT_ON);
    digitalWrite(SEGG, SEGMENT_ON);
    return 5;

  case 6:
    digitalWrite(SEGA, SEGMENT_ON);
    digitalWrite(SEGB, SEGMENT_OFF);
    digitalWrite(SEGC, SEGMENT_ON);
    digitalWrite(SEGD, SEGMENT_ON);
    digitalWrite(SEGE, SEGMENT_ON);
    digitalWrite(SEGF, SEGMENT_ON);
    digitalWrite(SEGG, SEGMENT_ON);
    return 6;

  case 7:
    digitalWrite(SEGA, SEGMENT_ON);
    digitalWrite(SEGB, SEGMENT_ON);
    digitalWrite(SEGC, SEGMENT_ON);
    digitalWrite(SEGD, SEGMENT_OFF);
    digitalWrite(SEGE, SEGMENT_OFF);
    digitalWrite(SEGF, SEGMENT_OFF);
    digitalWrite(SEGG, SEGMENT_OFF);
    return 3;

  case 8:
    digitalWrite(SEGA, SEGMENT_ON);
    digitalWrite(SEGB, SEGMENT_ON);
    digitalWrite(SEGC, SEGMENT_ON);
    digitalWrite(SEGD, SEGMENT_ON);
    digitalWrite(SEGE, SEGMENT_ON);
    digitalWrite(SEGF, SEGMENT_ON);
    digitalWrite(SEGG, SEGMENT_ON);
    return 7;

  case 9:
    digitalWrite(SEGA, SEGMENT_ON);
    digitalWrite(SEGB, SEGMENT_ON);
    digitalWrite(SEGC, SEGMENT_ON);
    digitalWrite(SEGD, SEGMENT_ON);
    digitalWrite(SEGE, SEGMENT_OFF);
    digitalWrite(SEGF, SEGMENT_ON);
    digitalWrite(SEGG, SEGMENT_ON);
    return 6;

  default:
    digitalWrite(SEGA, SEGMENT_OFF);
    digitalWrite(SEGB, SEGMENT_OFF);
    digitalWrite(SEGC, SEGMENT_OFF);
    digitalWrite(SEGD, SEGMENT_OFF);
    digitalWrite(SEGE, SEGMENT_OFF);
    digitalWrite(SEGF, SEGMENT_OFF);
    digitalWrite(SEGG, SEGMENT_OFF);
    return 0;
  }
}
