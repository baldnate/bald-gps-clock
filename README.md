bald-gps-clock
==============

My attempt to build a block based on the following parts:

* An ATMEGA168 I have laying around
* A GPS radio that talks over serial
* A 4 digit 7 segment display
* A photo cell

As it turns out, there are just enough pins on the micro to support:

* The all four digits of the seven segment (but not the decimal points)
* The colon and the AM/PM dot
* Talking to the GPS radio
* 3 pushbuttons for interaction
* A photocell for auto-dimming

The pushbuttons are pull-high on contact, and are wired to the 3 ICSP communication pins.

The end result (hopefully) will be the following:

* A GPS sourced clock with auto-dimming display
* Push button for hour offset adjust (to handle time-zones and DST)
* Push button for mode (time, date, etc)
* One extra push button for now
* All put on a nicely layed-out PCB